$(document).ready(function(){

   // var itaImgLink = "img/countries/Italien.gif";
    var engImgLink = "img/countries/Grossbritanien.gif";
    var deuImgLink = "img/countries/Deutschland.gif";
   // var fraImgLink = "img/countries/Frankreich.gif";
    var esImgLink  = "img/countries/Spanien.gif";
    var ptImgLink  = "img/countries/Portugal.gif";

    var imgBtnSel = $('#imgBtnSel');
    // var imgBtnIta = $('#imgBtnIta');
    var imgBtnEng = $('#imgBtnEng');
    var imgBtnDeu = $('#imgBtnDeu');
    // var imgBtnFra = $('#imgBtnFra');
    var imgBtnEs  = $('#imgBtnEs');
    var imgBtnPt  = $('#imgBtnPt');

    var spanBtnSel = $('#lanBtnSel');

    imgBtnSel.attr("src",engImgLink);

    // imgBtnIta.attr("src",itaImgLink);
    imgBtnEng.attr("src",engImgLink);
    spanBtnSel.text("EN");
    i18n.changeLanguage('en-US');

    imgBtnDeu.attr("src",deuImgLink);
    // imgBtnFra.attr("src",fraImgLink);
    imgBtnEs.attr("src",esImgLink);
    imgBtnPt.attr("src",ptImgLink);

    $( ".language" ).on( "click", function( event ) {
        var currentId = $(this).attr('id');

        /*if(currentId == "btnIta") {
            imgBtnSel.attr("src",itaImgLink);
            spanBtnSel.text("IT");
            i18n.changeLanguage('it');
        } else*/ if (currentId == "btnEng") {
            imgBtnSel.attr("src",engImgLink);
            spanBtnSel.text("EN");
            i18n.changeLanguage('en-US');
        } else if (currentId == "btnDeu") {
            imgBtnSel.attr("src",deuImgLink);
            spanBtnSel.text("DE");
            i18n.changeLanguage('de');
        } /*else if (currentId == "btnFra") {
            imgBtnSel.attr("src",fraImgLink);
            spanBtnSel.text("FR");
            i18n.changeLanguage('fr');
        }*/ else if (currentId == "btnEs") {
            imgBtnSel.attr("src",esImgLink);
            spanBtnSel.text("ES");
            i18n.changeLanguage('es');
        } else if (currentId == "btnPt") {
            imgBtnSel.attr("src",ptImgLink);
            spanBtnSel.text("PT");
            i18n.changeLanguage('pt');
        }

        
    });
});