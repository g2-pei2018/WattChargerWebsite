  // Google Maps Scripts
  google.maps.event.addDomListener(window, 'load', initMap);
  google.maps.event.addDomListener(window, 'resize', initMap);
  
  function initMap() {
    var image = 'img/map-marker.svg';
    var myLatLng = {lat: 41.559897, lng: -8.3988117};
    // 41.559897, -8.3988117 => Braga, Minho University

    // Get the HTML DOM element that will contain your map
    // We are using a div with id="map" seen below in the <body>
    var mapElement = document.getElementById('map');
    var mapOptions = {
      zoom: 15,
      center: myLatLng
    };

    var mapOptions2 = {
        // How zoomed in you want the map to start at (always required)
        zoom: 15,
        backgroundColor: 'hsla(0, 0%, 0%, 0)',

        // The latitude and longitude to center the map (always required)
        center:  myLatLng,

        // Disables the default Google Maps UI components
        disableDefaultUI: true,
        scrollwheel: false,
        draggable: false,

        // How you would like to style the map.
        // This is where you would paste any style found on Snazzy Maps.
        styles: [{
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [{
            "color": "#ffffff"
          }, {
            "lightness": 17
          }]
        },     {
          "featureType": "landscape",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      }, {
          "featureType": "road.highway",
          "elementType": "geometry.fill",
          "stylers": [{
            "color": "#000000"
          }, {
            "lightness": 17
          }]
        }, {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [{
            "color": "#000000"
          }, {
            "lightness": 29
          }, {
            "weight": 0.2
          }]
        }, {
          "featureType": "road.arterial",
          "elementType": "geometry",
          "stylers": [{
            "color": "#000000"
          }, {
            "lightness": 18
          }]
        }, {
          "featureType": "road.local",
          "elementType": "geometry",
          "stylers": [{
            "color": "#000000"
          }, {
            "lightness": 16
          }]
        }, {
          "featureType": "poi",
          "elementType": "geometry",
          "stylers": [              {
            "visibility": "off"
        }]
        }, {
          "elementType": "labels.text.stroke",
          "stylers": [{
            "visibility": "on"
          }, {
            "color": "#000000"
          }, {
            "lightness": 16
          }]
        }, {
          "elementType": "labels.text.fill",
          "stylers": [{
            "saturation": 36
          }, {
            "color": "#58C892"
          }, {
            "lightness": 40
          }]
        }, {
          "elementType": "labels.icon",
          "stylers": [{
            "visibility": "off"
          }]
        }, {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [{
            "color": "#58C892"
          }, {
            "lightness": 19
          }]
        }, {
          "featureType": "administrative",
          "elementType": "geometry.fill",
          "stylers": [{
            "color": "#58C892"
          }, {
            "lightness": 20
          }]
        },     {
          "featureType": "administrative",
          "stylers": [
              {
                  "visibility": "off"
              }
          ]
      }]
      };
    
    var map = new google.maps.Map(mapElement, mapOptions2);

    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      icon: image 
    });
}

  